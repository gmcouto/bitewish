package bitewish.domain.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import bitewish.BitewishApplication;
import bitewish.domain.model.BitePoll;
import bitewish.domain.model.BitePollResult;
import bitewish.domain.model.Place;
import bitewish.domain.repository.BitePollRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BitewishApplication.class)
@WebAppConfiguration
public class BitePollsServiceTests {
	@Mock
	BitePollRepository bitePollRepository;

	@Autowired
	@InjectMocks
	BitePollsService bitePollsService;

	@Test
	public void givenABitePollsServiceWithNoPreviousBitePollWhenGetOneAndCreateIfDoesntExistsThenReturnsANewBitePoll() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(bitePollRepository.save(Matchers.any(BitePoll.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		Assert.assertNotNull(bitePollsService
				.getOneAndCreateIfDoesntExists("01/01/2015"));

		Mockito.verify(bitePollRepository).findFirstByDate(
				Matchers.any(LocalDate.class));
		Mockito.verify(bitePollRepository).save(Matchers.any(BitePoll.class));
		Mockito.verifyNoMoreInteractions(bitePollRepository);
	}

	@Test
	public void givenABitePollsServiceWithPreviousBitePollWhenGetOneAndCreateIfDoesntExistsThenReturnsANewBitePoll() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(
				bitePollRepository.findFirstByDate(Matchers
						.any(LocalDate.class))).thenReturn(new BitePoll(null));

		Assert.assertNotNull(bitePollsService
				.getOneAndCreateIfDoesntExists("01/01/2015"));

		Mockito.verify(bitePollRepository).findFirstByDate(
				Matchers.any(LocalDate.class));
		Mockito.verifyNoMoreInteractions(bitePollRepository);
	}

	@Test
	public void givenABitePollsServiceWithPreviousBitePollWhenCalculateWinnerIsCalledThenReturnsABitePollWithAWinner() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(
				bitePollRepository.findFirstByDate(Matchers
						.any(LocalDate.class))).thenReturn(new BitePoll(null));

		List<BitePollResult> pollResults = new ArrayList<BitePollResult>();
		pollResults.add(new BitePollResult(new Place("Panorama"), 10L));
		pollResults.add(new BitePollResult(new Place("Palatus"), 5L));

		Mockito.when(
				bitePollRepository.findBitePollResults(Matchers
						.any(LocalDate.class))).thenReturn(pollResults);

		Mockito.when(bitePollRepository.save(Matchers.any(BitePoll.class)))
				.thenAnswer(AdditionalAnswers.returnsFirstArg());

		Assert.assertNotNull(bitePollsService.calculateWinner("01/01/2015")
				.getGrantedWish());

		Mockito.verify(bitePollRepository).findFirstByDate(
				Matchers.any(LocalDate.class));
		Mockito.verify(bitePollRepository).findBitePollResults(
				Matchers.any(LocalDate.class));
		Mockito.verify(bitePollRepository).save(Matchers.any(BitePoll.class));
		Mockito.verifyNoMoreInteractions(bitePollRepository);
	}
}
