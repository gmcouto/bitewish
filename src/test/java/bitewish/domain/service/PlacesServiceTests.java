package bitewish.domain.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import bitewish.BitewishApplication;
import bitewish.domain.model.Place;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BitewishApplication.class)
@WebAppConfiguration
public class PlacesServiceTests {
	@Autowired
	PlacesService placesService;

	@Test
	public void givenAPlacesServiceWhenCalledCreatePlaceWithANameThenReturnsAPlaceWithThatName() {
		Place aPlace = placesService.createPlace("name");
		Assert.assertEquals("name", aPlace.getName());
		Assert.assertNotNull(aPlace.getId());
	}

	@Test
	public void givenAPlacesServiceWhenCalledGetOnePlaceWithAnIdThenReturnsAPlaceWithThatId() {
		Place aPlace = placesService.createPlace("name");
		Place aPlaceFromGetOne = placesService.getOnePlace(aPlace.getId());
		Assert.assertEquals("name", aPlaceFromGetOne.getName());
		Assert.assertNotNull(aPlaceFromGetOne.getId());
	}

	@Test
	public void givenAPlacesServiceWhenCalledGetAllPlacesAvailableForVotingReturnsAPlaceInsertedBecauseItWasNeverUsed() {
		Place aPlace = placesService.createPlace("name");
		List<Place> placesList = placesService
				.getAllPlacesAvailableForVoting("15/05/2015");
		Assert.assertTrue(placesList.contains(aPlace));
	}
}
