package bitewish.domain.service;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import bitewish.BitewishApplication;
import bitewish.domain.model.BitePoll;
import bitewish.domain.model.Place;
import bitewish.domain.model.User;
import bitewish.domain.model.Vote;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BitewishApplication.class)
@WebAppConfiguration
public class VoteServiceTests {
	@Autowired
	VoteService voteService;

	@Autowired
	UsersService usersService;

	@Autowired
	BitePollsService bitePollsService;

	@Autowired
	PlacesService placesService;

	@Test
	public void givenAVoteServiceWhenCalledCreateVoteWithNecessaryParametersThenReturnAVoteWithAnId() {
		User aUser = usersService.createUser("name");
		BitePoll bitePoll = bitePollsService
				.getOneAndCreateIfDoesntExists("01/01/2001");
		Place aPlace = placesService.createPlace("name");
		Vote aVote = voteService.createVote(bitePoll, aUser, aPlace);
		Assert.assertNotNull(aVote.getId());
	}

	@Test
	public void givenAVoteServiceWhenCalledGetUserCurrentVoteWithNecessaryParametersThenReturnAVoteAfterVoteIsCreated() {
		User aUser = usersService.createUser("name");
		BitePoll bitePoll = bitePollsService
				.getOneAndCreateIfDoesntExists("01/01/2001");
		Place aPlace = placesService.createPlace("name");
		Optional<Vote> voteFound = voteService.getUserCurrentVote(aUser,
				bitePoll);
		Assert.assertFalse(voteFound.isPresent());
		voteService.createVote(bitePoll, aUser, aPlace);
		voteFound = voteService.getUserCurrentVote(aUser, bitePoll);
		Assert.assertTrue(voteFound.isPresent());
	}

	@Test
	public void givenAVoteServiceWhenCalledCountVotesFromDateAfterCreateVoteThenReturnANumberGreaterThanBefore() {
		User aUser = usersService.createUser("name");
		BitePoll bitePoll = bitePollsService
				.getOneAndCreateIfDoesntExists("01/01/2001");
		Place aPlace = placesService.createPlace("name");
		Long before = voteService.countVotesFromDate("01/01/2001");
		voteService.createVote(bitePoll, aUser, aPlace);
		Long after = voteService.countVotesFromDate("01/01/2001");
		Assert.assertTrue(after > before);
	}
}
