package bitewish.domain.service;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Test;

public class DateServiceTests {
	DateService dateService = new DateServiceImpl();

	@Test
	public void givenADateServiceWhenCalledGetLocalDateFromStringThenReturnIsACompatibleDate() {
		LocalDate date = dateService.getLocalDateFromString("01/02/2002");
		Assert.assertEquals(01, date.getDayOfMonth());
		Assert.assertEquals(02, date.getMonthValue());
		Assert.assertEquals(2002, date.getYear());
	}

	@Test
	public void givenADateServiceWhenCalledGetLocalDateTimeFromStringThenReturnIsACompatibleDate() {
		LocalDateTime dateTime = dateService
				.getLocalDateTimeFromString("01/02/2002 09:30");
		Assert.assertEquals(01, dateTime.getDayOfMonth());
		Assert.assertEquals(02, dateTime.getMonthValue());
		Assert.assertEquals(2002, dateTime.getYear());
		Assert.assertEquals(9, dateTime.getHour());
		Assert.assertEquals(30, dateTime.getMinute());
	}

	@Test
	public void givenADateServiceWhenCalledGetStringFromLocalDateThenReturnIsACompatibleDate() {
		String dateTest = "01/02/2002";
		LocalDate date = dateService.getLocalDateFromString(dateTest);
		Assert.assertEquals(dateTest, dateService.getStringFromLocalDate(date));
	}

	@Test
	public void givenADateServiceWhenCalledGetStringFromLocalDateTimeThenReturnIsACompatibleDate() {
		String dateTest = "01/02/2002 09:30";
		LocalDateTime dateTime = dateService
				.getLocalDateTimeFromString(dateTest);
		Assert.assertEquals(dateTest,
				dateService.getStringFromLocalDateTime(dateTime));
	}

	@Test
	public void givenADateServiceWhenCalledGetMondayOfTheWeekThenReturnIsACompatibleDate() {
		String dateTest = "16/06/2015";
		LocalDate date = dateService.getLocalDateFromString(dateTest);
		date = dateService.getMondayOfTheWeek(date);
		Assert.assertEquals(15, date.getDayOfMonth());
		Assert.assertEquals(06, date.getMonthValue());
		Assert.assertEquals(2015, date.getYear());
	}

	@Test
	public void givenADateServiceWhenCalledGetFridayOfTheWeekThenReturnIsACompatibleDate() {
		String dateTest = "16/06/2015";
		LocalDate date = dateService.getLocalDateFromString(dateTest);
		date = dateService.getFridayOfTheWeek(date);
		Assert.assertEquals(19, date.getDayOfMonth());
		Assert.assertEquals(06, date.getMonthValue());
		Assert.assertEquals(2015, date.getYear());
	}
}
