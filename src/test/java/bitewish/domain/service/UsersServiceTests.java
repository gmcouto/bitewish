package bitewish.domain.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import bitewish.BitewishApplication;
import bitewish.domain.model.User;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BitewishApplication.class)
@WebAppConfiguration
public class UsersServiceTests {
	@Autowired
	UsersService usersService;

	@Test
	public void givenAUsersServiceWhemCalledCreateUserWithANameThenReturnsAUserWithProperNameAndId() {
		User aUser = usersService.createUser("name");
		Assert.assertEquals("name", aUser.getName());
		Assert.assertNotNull(aUser.getId());
	}

	@Test
	public void givenAUsersServiceWhemCalledUserCountAfterAddingUserThenValueMustHaveIncreased() {
		Long initial = usersService.userCount();
		usersService.createUser("name");
		Assert.assertTrue(usersService.userCount() > initial);
	}
}
