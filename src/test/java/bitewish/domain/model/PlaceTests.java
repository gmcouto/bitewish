package bitewish.domain.model;

import org.junit.Assert;
import org.junit.Test;

public class PlaceTests {
	@Test
	public void givenAPlaceWithNamePanoramaThenGetNameReturnsPanorama() {
		Place aPlace = new Place("Panorama");
		Assert.assertEquals("Panorama", aPlace.getName());
	}

	@Test
	public void givenAPlaceWithNamePanoramaThenToStringReturnsPanorama() {
		Place aPlace = new Place("Panorama");
		Assert.assertEquals("Panorama", aPlace.toString());
	}
}
