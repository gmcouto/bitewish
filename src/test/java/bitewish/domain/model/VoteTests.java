package bitewish.domain.model;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class VoteTests {
	@Test
	public void givenAVoteWithABitePollThenGetBitePollReturnTheSame() {
		BitePoll aBitePoll = Mockito.mock(BitePoll.class);
		Mockito.when(aBitePoll.getId()).thenReturn(1L);
		Vote aVote = new Vote(aBitePoll, null, null);
		Assert.assertEquals(aBitePoll.getId(), aVote.getBitePoll().getId());
	}

	@Test
	public void givenAVoteWithAUserThenGetUserReturnTheSame() {
		User aUser = Mockito.mock(User.class);
		Mockito.when(aUser.getId()).thenReturn(1L);
		Vote aVote = new Vote(null, aUser, null);
		Assert.assertEquals(aUser.getId(), aVote.getUser().getId());
	}

	@Test
	public void givenAVoteWithAPlaceThenGetPlaceReturnTheSame() {
		Place aPlace = Mockito.mock(Place.class);
		Mockito.when(aPlace.getId()).thenReturn(1L);
		Vote aVote = new Vote(null, null, aPlace);
		Assert.assertEquals(aPlace.getId(), aVote.getPlace().getId());
	}
}
