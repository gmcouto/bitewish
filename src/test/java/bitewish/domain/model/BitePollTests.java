package bitewish.domain.model;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class BitePollTests {
	@Test
	public void givenABitePollWithSpecificDateThenGetDateReturnSameDate() {
		LocalDate now = LocalDate.now();
		BitePoll aBitePoll = new BitePoll(now);
		Assert.assertEquals(now, aBitePoll.getDate());
	}

	@Test
	public void givenABitePollWithGrantedWishThenGetGrantedWishReturnsSamePlace() {
		Place aPlace = Mockito.mock(Place.class);
		Mockito.when(aPlace.getId()).thenReturn(1L);
		BitePoll aBitePoll = new BitePoll(null);
		aBitePoll.setGrantedWish(aPlace);
		Assert.assertEquals(aPlace.getId(), aBitePoll.getGrantedWish().getId());
	}
}
