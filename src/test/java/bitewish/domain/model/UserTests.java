package bitewish.domain.model;

import org.junit.Assert;
import org.junit.Test;

public class UserTests {
	@Test
	public void givenAUserWithNameAntonioThenGetNameReturnsAntonio() {
		User aUser = new User("Antonio");
		Assert.assertEquals("Antonio", aUser.getName());
	}

	@Test
	public void givenAUserWithNameAntonioThenToStringReturnsAntonio() {
		User aUser = new User("Antonio");
		Assert.assertEquals("Antonio", aUser.toString());
	}

	@Test
	public void givenAUserWithNameAntonioAndSetNameAntoniaThenGetNameReturnsAntonia() {
		User aUser = new User("Antonio");
		aUser.setName("Antonia");
		Assert.assertEquals("Antonia", aUser.getName());
	}
}
