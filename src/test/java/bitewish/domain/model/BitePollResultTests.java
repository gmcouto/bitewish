package bitewish.domain.model;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class BitePollResultTests {
	@Test
	public void givenABitePollResultWithAPlaceThenGetPlaceReturnTheSame() {
		Place aPlace = Mockito.mock(Place.class);
		Mockito.when(aPlace.getId()).thenReturn(1L);
		BitePollResult bitePollResult = new BitePollResult(aPlace, null);
		Assert.assertEquals(aPlace.getId(), bitePollResult.getPlace().getId());
	}

	@Test
	public void givenABitePollResultWithANumberOfVotesThenGetCountReturnTheSame() {
		BitePollResult bitePollResult = new BitePollResult(null, 12L);
		Assert.assertEquals((Long) 12L, bitePollResult.getCount());
	}
}
