package bitewish.app.service;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import bitewish.BitewishApplication;
import bitewish.domain.service.BitePollsService;
import bitewish.domain.service.DateService;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BitewishApplication.class)
@WebAppConfiguration
public class DailyBitePollServiceTests {
	@Autowired
	DailyBitePollService dailyBitePollService;

	@Autowired
	DateService dateService;

	@Autowired
	BitePollsService bitePollsService;

	@Test
	public void givenADailyBitePollServiceWhenCalledGetCurrentDayAsStringThenReturnTheCorrectStringOfTheDay() {
		String today = dateService.getStringFromLocalDate(LocalDate.now());
		Assert.assertEquals(today, dailyBitePollService.getCurrentDayAsString());
	}

	@Test
	public void givenADailyBitePollServiceWhenCalledGetTodaysBitePollThenReturnTheSameBitePollFromBitePollServiceForThatDate() {
		String today = dateService.getStringFromLocalDate(LocalDate.now());
		Assert.assertEquals(
				bitePollsService.getOneAndCreateIfDoesntExists(today),
				dailyBitePollService.getTodaysBitePoll());
	}
}
