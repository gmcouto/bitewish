package bitewish.app.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import bitewish.BitewishApplication;
import bitewish.app.service.UserSessionService;
import bitewish.domain.model.Place;
import bitewish.domain.model.User;
import bitewish.domain.service.PlacesService;
import bitewish.domain.service.UsersService;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BitewishApplication.class)
@WebAppConfiguration
public class AppVotingControllerTests {
	@Mock
	UserSessionService userSessionService;

	@Autowired
	UsersService usersService;

	@Autowired
	PlacesService placesService;

	@Autowired
	@InjectMocks
	AppVotingController appVotingController;

	@Before
	public void setUp() {
		User aUser = usersService.createUser("name");
		MockitoAnnotations.initMocks(this);
		Mockito.when(userSessionService.getCurrentUser()).thenReturn(aUser);
	}

	@Test
	public void givenAAppVotingControllerWhenCalledHomeWithUserNOTLoggedInThenReturnedModelShouldBeLogin() {
		Mockito.when(userSessionService.getCurrentUser()).thenReturn(null);
		ModelAndView mav = appVotingController.home();
		Assert.assertEquals("login", mav.getViewName());
	}

	@Test
	public void givenAAppVotingControllerWhenCalledHomeWithLoggedInUserThenReturnedModelShouldBeMakewish() {
		ModelAndView mav = appVotingController.home();
		Assert.assertEquals("makewish", mav.getViewName());
	}

	@Test
	public void givenAAppVotingControllerWhenCalledVoteWithUserNOTLoggedInThenReturnedModelShouldBeDone() {
		Mockito.when(userSessionService.getCurrentUser()).thenReturn(null);
		Place aPlace = placesService.createPlace("name");
		ModelAndView mav = appVotingController.vote(aPlace.getId());
		Assert.assertEquals("login", mav.getViewName());
	}
}
