package bitewish.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import bitewish.app.service.DailyBitePollService;
import bitewish.app.service.UserSessionService;
import bitewish.domain.model.BitePoll;
import bitewish.domain.model.Place;
import bitewish.domain.model.User;
import bitewish.domain.model.Vote;
import bitewish.domain.service.PlacesService;
import bitewish.domain.service.VoteService;

@Controller
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class AppVotingController {
	@Autowired
	PlacesService placesService;

	@Autowired
	VoteService votesService;

	@Autowired
	UserSessionService userSessionService;

	@Autowired
	DailyBitePollService dailyBitePollService;

	@ModelAttribute("todaysPoll")
	public BitePoll todaysPoll() {
		return dailyBitePollService.getTodaysBitePoll();
	}

	@ModelAttribute("availablePlacesToVote")
	public Iterable<Place> availablePlacesToVote() {
		return placesService
				.getAllPlacesAvailableForVoting(dailyBitePollService
						.getCurrentDayAsString());
	}

	@ModelAttribute("grantedWish")
	public Optional<Place> grantedWish() {
		return Optional.ofNullable(todaysPoll().getGrantedWish());
	}

	@ModelAttribute("currentUser")
	public Optional<User> currentUser() {
		return Optional.ofNullable(userSessionService.getCurrentUser());
	}

	@ModelAttribute("userVote")
	public Optional<Vote> userVote() {
		if (currentUser().isPresent()) {
			return votesService.getUserCurrentVote(currentUser().get(),
					todaysPoll());
		}
		return Optional.empty();
	}

	@ModelAttribute("minutesRemaining")
	public long minutesRemaining() {
		return dailyBitePollService.remainingMinutesToFinish();
	}

	@ModelAttribute("votesRemaining")
	public long votesRemaining() {
		return dailyBitePollService.remainingVotesToFinish();
	}

	@RequestMapping(value = "/")
	public ModelAndView home() {
		if (!currentUser().isPresent())
			return new ModelAndView("login");

		if (grantedWish().isPresent())
			return showPartialResults();

		if (userVote().isPresent())
			return showWaitingForVotingCompletion();

		return showVotingOptions();
	}

	private ModelAndView showWaitingForVotingCompletion() {
		return new ModelAndView("waiting");
	}

	private ModelAndView showPartialResults() {
		boolean isMyWish = false;
		if (userVote().isPresent() && grantedWish().isPresent())
			isMyWish = userVote().get().getPlace().getId()
					.equals(grantedWish().get().getId());
		return new ModelAndView("result").addObject("isMyWish", isMyWish);
	}

	private ModelAndView showVotingOptions() {
		return new ModelAndView("makewish");
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView vote(
			@RequestParam(required = true, value = "votedPlace") Long place_id) {
		if (!currentUser().isPresent())
			return new ModelAndView("login");

		if (userVote().isPresent())
			return new ModelAndView("redirect:/");

		Place votedPlace = placesService.getOnePlace(place_id);
		votesService.createVote(todaysPoll(), currentUser().get(), votedPlace);

		if (grantedWish().isPresent())
			return new ModelAndView("redirect:/");

		return new ModelAndView("done").addObject("userVote", userVote())
				.addObject("minutesRemaining", minutesRemaining())
				.addObject("votesRemaining", votesRemaining());
	}
}
