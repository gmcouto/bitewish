package bitewish.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import bitewish.app.model.BitePollGraphResult;
import bitewish.app.service.DailyBitePollService;

@RestController
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class RestResultController {
	@Autowired
	DailyBitePollService dailyBitePollService;

	@RequestMapping(value = "/results")
	public Iterable<BitePollGraphResult> getResults() {
		return dailyBitePollService.getTodaysGraphResult();

	}
}
