package bitewish.app.model;

public class BitePollGraphResult {
	private Long value;
	private String label;

	public BitePollGraphResult(Long value, String label) {
		super();
		this.value = value;
		this.label = label;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
