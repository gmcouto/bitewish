package bitewish.app.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import bitewish.app.model.BitePollGraphResult;
import bitewish.domain.model.BitePoll;
import bitewish.domain.model.BitePollResult;
import bitewish.domain.service.BitePollsService;
import bitewish.domain.service.DateService;
import bitewish.domain.service.UsersService;
import bitewish.domain.service.VoteService;

@Profile("production")
@Service
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class DailyBitePollServiceImpl implements DailyBitePollService {

	@Autowired
	BitePollsService bitePollsService;

	@Autowired
	DateService dateService;

	@Autowired
	VoteService voteService;

	@Autowired
	UsersService usersService;

	@Override
	public LocalDateTime getCurrentTime() {
		return LocalDateTime.now();
	}

	@Override
	public BitePoll getTodaysBitePoll() {
		return getBitePollAndCheckForAvailabilityForVotingOrFinishItIfDone(getCurrentDayAsString());
	}

	@Override
	public String getCurrentDayAsString() {
		return dateService.getStringFromLocalDate(LocalDate
				.from(getCurrentTime()));
	}

	@Override
	public List<BitePollGraphResult> getTodaysGraphResult() {
		List<BitePollGraphResult> graphResultList = new ArrayList<BitePollGraphResult>();
		bitePollsService.getBitePollPartialResult(getCurrentDayAsString())
				.forEach(
						result -> {
							graphResultList.add(new BitePollGraphResult(result
									.getCount(), result.getPlace().getName()));
						});
		return graphResultList;
	}

	@Override
	public long remainingMinutesToFinish() {
		LocalDateTime now = getCurrentTime();
		LocalDateTime voteEnd = getEndingTime(now);
		return now.until(voteEnd, ChronoUnit.MINUTES);
	}

	@Override
	public long remainingVotesToFinish() {
		return usersService.userCount()
				- voteService.countVotesFromDate(getCurrentDayAsString());
	}

	private LocalDateTime getEndingTime(LocalDateTime aTime) {
		return aTime.withHour(11).withMinute(30).withSecond(0);
	}

	private BitePoll getBitePollAndCheckForAvailabilityForVotingOrFinishItIfDone(
			String bitePollDate) {
		BitePoll todaysPoll = bitePollsService
				.getOneAndCreateIfDoesntExists(bitePollDate);
		LocalDateTime now = getCurrentTime();
		LocalDateTime voteEnd = getEndingTime(now);
		long votesFromCurrentPoll = voteService
				.countVotesFromDate(bitePollDate);
		long allUsers = usersService.userCount();
		long votesAvailable = allUsers - votesFromCurrentPoll;
		long firstMostVoted = 0;
		long secondMostVoted = 0;
		boolean enoughVotedForDecision = votesAvailable == 0;

		if (!enoughVotedForDecision) {
			List<BitePollResult> partials = bitePollsService
					.getBitePollPartialResult(bitePollDate);
			if (partials.size() >= 1) {
				firstMostVoted = partials.get(0).getCount();
				if (partials.size() >= 2)
					secondMostVoted = partials.get(1).getCount();

				if ((firstMostVoted > secondMostVoted)
						&& (firstMostVoted - secondMostVoted) > votesAvailable)
					enoughVotedForDecision = true;
			}
		}

		boolean timeIsOver = now.isAfter(voteEnd);
		if (todaysPoll.getGrantedWish() == null
				&& (enoughVotedForDecision || timeIsOver)) {
			todaysPoll = bitePollsService.calculateWinner(bitePollDate);
		}
		return todaysPoll;
	}
}
