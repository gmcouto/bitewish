package bitewish.app.service;

import bitewish.domain.model.User;

public interface UserSessionService {
	public User getCurrentUser();
}
