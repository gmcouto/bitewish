package bitewish.app.service;

import java.time.LocalDateTime;
import java.util.List;

import bitewish.app.model.BitePollGraphResult;
import bitewish.domain.model.BitePoll;

public interface DailyBitePollService {
	public LocalDateTime getCurrentTime();

	public String getCurrentDayAsString();

	public BitePoll getTodaysBitePoll();

	public List<BitePollGraphResult> getTodaysGraphResult();

	public long remainingMinutesToFinish();

	public long remainingVotesToFinish();
}
