package bitewish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BitewishApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitewishApplication.class, args);
	}
}
