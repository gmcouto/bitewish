package bitewish.fake;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import bitewish.domain.model.User;
import bitewish.domain.repository.UserRepository;
import bitewish.domain.service.DateService;

@Controller
@Scope(WebApplicationContext.SCOPE_SESSION)
@Profile(value = "demo")
public class CheatController {
	@Autowired
	FakeData fakeData;

	@Autowired
	UserRepository userRepository;

	@Autowired
	FakeDailyBitePollServiceImpl dailyBitePollService;

	@Autowired
	FakeUserSessionServiceImpl userSessionService;

	@Autowired
	DateService dateService;

	private Iterable<User> allUsers() {
		return userRepository.findAllByOrderByNameAsc();
	}

	private Optional<User> currentUser() {
		return Optional.ofNullable(userSessionService.getCurrentUser());
	}

	private CheatForm currentCheatState() {
		CheatForm state = new CheatForm();
		if (currentUser().isPresent())
			state.setCurrentUser(currentUser().get());
		state.setDateChosen(dailyBitePollService.getCurrentTime());
		return state;
	}

	@RequestMapping(value = { "/cheat", "/login", "/logout" })
	public ModelAndView cheatOptions() {
		return new ModelAndView("cheat").addObject("cheatState",
				currentCheatState()).addObject("allUsers", allUsers());
	}

	@RequestMapping(value = { "/cheat", "/login", "/logout" }, method = RequestMethod.POST)
	public ModelAndView startCheating(
			@Valid @ModelAttribute("cheatState") CheatForm cheatForm,
			BindingResult bindingResult, Model m) {
		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = cheatOptions();
			return modelAndView.addObject("cheatState", cheatForm);
		} else {
			dailyBitePollService.setToday(cheatForm.getDateChosen());
			userSessionService.setUser(cheatForm.getCurrentUser());
			return new ModelAndView("redirect:/");
		}

	}
}
