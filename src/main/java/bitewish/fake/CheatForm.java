package bitewish.fake;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import bitewish.domain.model.User;

public class CheatForm {
	@NotNull
	private User currentUser;
	@NotNull(message="This date is invalid. Make sure it is dd/MM/yyyy HH:mm")
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime dateChosen;

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public LocalDateTime getDateChosen() {
		return dateChosen;
	}

	public void setDateChosen(LocalDateTime dateChosen) {
		this.dateChosen = dateChosen;
	}

}
