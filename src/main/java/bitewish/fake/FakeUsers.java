package bitewish.fake;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import bitewish.domain.service.UsersService;

@Component
@Profile(value = "demo")
public class FakeUsers {
	@Autowired
	UsersService usersService;

	@PostConstruct
	public void createFakeUsers() {
		usersService.createUser("Ana");
		usersService.createUser("Antônio");
		usersService.createUser("Janaína");
		// usersService.createUser("João");
		// usersService.createUser("Carolina");
		// usersService.createUser("Caio");
	}
}
