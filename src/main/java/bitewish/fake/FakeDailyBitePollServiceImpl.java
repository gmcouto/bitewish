package bitewish.fake;

import java.time.LocalDateTime;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import bitewish.app.service.DailyBitePollServiceImpl;

@Service
@Profile(value = "demo")
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class FakeDailyBitePollServiceImpl extends DailyBitePollServiceImpl {

	private LocalDateTime now = LocalDateTime.now();

	@Override
	public LocalDateTime getCurrentTime() {
		return now;
	}

	public void setToday(LocalDateTime today) {
		this.now = today;
	}

}
