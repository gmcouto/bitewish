package bitewish.fake;

import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import bitewish.app.service.UserSessionService;
import bitewish.domain.model.User;

@Service
@Profile(value = "demo")
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class FakeUserSessionServiceImpl implements UserSessionService {
	private User user;

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public User getCurrentUser() {
		return user;
	}

}
