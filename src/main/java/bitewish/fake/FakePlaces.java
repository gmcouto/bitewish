package bitewish.fake;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import bitewish.domain.service.PlacesService;

@Component
@Profile(value = "demo")
public class FakePlaces {
	@Autowired
	PlacesService placesService;

	@PostConstruct
	public void createFakePlaces() {
		placesService.createPlace("Palatus");
		placesService.createPlace("Panorama");
		placesService.createPlace("Subway");
		placesService.createPlace("Graxinha");
		placesService.createPlace("RU");
	}
}
