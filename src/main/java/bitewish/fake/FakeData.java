package bitewish.fake;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile(value = "demo")
public class FakeData {
	@Autowired
	FakePlaces fakePlaces;

	@Autowired
	FakeUsers fakeUsers;
}
