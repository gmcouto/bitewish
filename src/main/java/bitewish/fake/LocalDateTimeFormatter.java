package bitewish.fake;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;

import bitewish.domain.service.DateService;

public class LocalDateTimeFormatter implements
		org.springframework.format.Formatter<LocalDateTime> {

	@Autowired
	DateService dateService;

	@Override
	public String print(LocalDateTime object, Locale locale) {
		return dateService.getStringFromLocalDateTime(object);
	}

	@Override
	public LocalDateTime parse(String text, Locale locale)
			throws ParseException {
		LocalDateTime localDateTime = null;
		try {
			localDateTime = dateService.getLocalDateTimeFromString(text);
		} catch (Exception e) {
			throw new ParseException(
					String.format(
							"Can't convert '%s' in LocalDateTime, additional info:\n%s",
							text, e.getMessage()), -1);
		}
		if (localDateTime == null)
			throw new ParseException(String.format(
					"Can't convert '%s' in LocalDateTime", text), -1);
		return localDateTime;
	}
}
