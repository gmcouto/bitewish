package bitewish.domain.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import bitewish.domain.model.BitePoll;
import bitewish.domain.model.User;
import bitewish.domain.model.Vote;

public interface VoteRepository extends CrudRepository<Vote, Long> {
	Optional<Vote> findFirstByUserAndBitePoll(User user, BitePoll bitePoll);

	Long countByBitePoll(BitePoll bitePoll);
}
