package bitewish.domain.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import bitewish.domain.model.BitePoll;
import bitewish.domain.model.BitePollResult;

public interface BitePollRepository extends CrudRepository<BitePoll, Long> {
	BitePoll findFirstByDate(LocalDate date);

	List<BitePoll> findByDateBetween(LocalDate start, LocalDate end);

	@Query(value = "select new bitewish.domain.model.BitePollResult(v.place,count(*) as voteCount) from Vote v join v.bitePoll b where b.date = :date group by v.place order by voteCount desc", countQuery = "select count(a) from A a")
	List<BitePollResult> findBitePollResults(@Param("date") LocalDate date);

}
