package bitewish.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import bitewish.domain.model.Place;

public interface PlaceRepository extends CrudRepository<Place, Long> {
	List<Place> findAllByOrderByNameAsc();
}
