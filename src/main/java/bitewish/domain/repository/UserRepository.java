package bitewish.domain.repository;

import org.springframework.data.repository.CrudRepository;

import bitewish.domain.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	Iterable<User> findAllByOrderByNameAsc();
}
