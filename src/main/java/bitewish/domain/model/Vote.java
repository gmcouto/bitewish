package bitewish.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "vote", uniqueConstraints = { @UniqueConstraint(columnNames = {
		"bite_poll_id", "user_id" }) })
public class Vote {

	@Id
	@GeneratedValue
	@Column(name = "vote_id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "bite_poll_id", nullable = false, updatable = false)
	private BitePoll bitePoll;

	@ManyToOne(optional = false)
	@JoinColumn(name = "user_id", nullable = false, updatable = false)
	private User user;

	@ManyToOne(optional = false)
	@JoinColumn(name = "place_id", nullable = false, updatable = false)
	private Place place;

	@SuppressWarnings("unused")
	private Vote() {
		// this is actually used by jpa implementation
	}

	public Vote(BitePoll bitePoll, User user, Place place) {
		super();
		this.bitePoll = bitePoll;
		this.user = user;
		this.place = place;
	}

	public Place getPlace() {
		return place;
	}

	public BitePoll getBitePoll() {
		return bitePoll;
	}

	public User getUser() {
		return user;
	}

	public Long getId() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof Vote) {
				if (((Vote) obj).getId() != null) {
					if (((Vote) obj).getId().equals(this.getId()))
						return true;
				}

			}
		}
		return false;
	}
}
