package bitewish.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "place")
public class Place {
	@Id
	@GeneratedValue
	@Column(name = "place_id")
	private Long id;

	@Column(name = "name", nullable = false, unique = true)
	private String name;

	@SuppressWarnings("unused")
	private Place() {
		//actually used by JPA implementation
	}

	public Place(String name) {
		super();
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof Place) {
				if (((Place) obj).getId() != null) {
					if (((Place) obj).getId().equals(this.getId()))
						return true;
				}

			}
		}
		return false;
	}
}
