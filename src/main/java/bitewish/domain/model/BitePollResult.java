package bitewish.domain.model;

public class BitePollResult {
	private Place place;
	private Long count;

	public BitePollResult(Place place, Long count) {
		super();
		this.place = place;
		this.count = count;
	}

	public Place getPlace() {
		return place;
	}

	public Long getCount() {
		return count;
	}

}
