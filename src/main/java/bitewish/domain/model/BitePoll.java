package bitewish.domain.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bite_poll")
public class BitePoll {
	@Id
	@GeneratedValue
	@Column(name = "bite_poll_id")
	private Long id;

	@Column(name = "date", unique = true)
	private LocalDate date;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "place_id")
	private Place grantedWish;

	@SuppressWarnings("unused")
	private BitePoll() {
		// this is actually used by jpa implementation
	}

	public BitePoll(LocalDate date) {
		super();
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public Place getGrantedWish() {
		return grantedWish;
	}

	public void setGrantedWish(Place grantedWish) {
		this.grantedWish = grantedWish;
	}

	public LocalDate getDate() {
		return date;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof BitePoll) {
				if (((BitePoll) obj).getId() != null) {
					if (((BitePoll) obj).getId().equals(this.getId()))
						return true;
				}

			}
		}
		return false;
	}
}
