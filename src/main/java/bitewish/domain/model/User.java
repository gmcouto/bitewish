package bitewish.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue
	@Column(name = "user_id", updatable = false)
	private Long id;
	@Column(name = "name", unique = true, nullable = false)
	// unique until a better representation of this entity comes
	private String name;

	@SuppressWarnings("unused")
	private User() {
		// this is actually used by jpa implementation
	}

	public User(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof User) {
				if (((User) obj).getId() != null) {
					if (((User) obj).getId().equals(this.getId()))
						return true;
				}

			}
		}
		return false;
	}
}
