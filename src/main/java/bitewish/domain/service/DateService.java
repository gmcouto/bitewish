package bitewish.domain.service;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface DateService {
	LocalDate getLocalDateFromString(String stringDate);

	String getStringFromLocalDate(LocalDate localDate);

	LocalDateTime getLocalDateTimeFromString(String stringLocalDateTime);

	String getStringFromLocalDateTime(LocalDateTime stringLocalDateTime);

	LocalDate getMondayOfTheWeek(LocalDate anyDate);

	LocalDate getFridayOfTheWeek(LocalDate anyDate);
}
