package bitewish.domain.service;

import java.util.List;

import bitewish.domain.model.Place;

public interface PlacesService {
	public Place createPlace(String name);

	public Place getOnePlace(Long place_id);

	public List<Place> getAllPlacesAvailableForVoting(String date);
}
