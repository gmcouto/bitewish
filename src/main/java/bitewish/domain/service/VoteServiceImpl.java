package bitewish.domain.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import bitewish.domain.model.BitePoll;
import bitewish.domain.model.Place;
import bitewish.domain.model.User;
import bitewish.domain.model.Vote;
import bitewish.domain.repository.VoteRepository;

@Component
public class VoteServiceImpl implements VoteService {

	@Autowired
	VoteRepository voteRepository;

	@Autowired
	BitePollsService bitePollsService;

	@Override
	public Vote createVote(BitePoll bitePoll, User votingUser, Place wish) {
		Vote newVote = new Vote(bitePoll, votingUser, wish);
		return voteRepository.save(newVote);
	}

	@Override
	public Long countVotesFromDate(String date) {
		return voteRepository.countByBitePoll(bitePollsService
				.getOneAndCreateIfDoesntExists(date));
	}

	@Override
	public Optional<Vote> getUserCurrentVote(User aUserThatVoted,
			BitePoll aPollToVerify) {
		return voteRepository.findFirstByUserAndBitePoll(aUserThatVoted,
				aPollToVerify);
	}
}
