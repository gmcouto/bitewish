package bitewish.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bitewish.domain.model.User;
import bitewish.domain.repository.UserRepository;

@Service
public class UsersServiceImpl implements UsersService {

	@Autowired
	UserRepository userRepository;

	@Override
	public User createUser(String name) {
		User newUser = new User(name);
		return userRepository.save(newUser);
	}

	@Override
	public Long userCount() {
		return userRepository.count();
	}

}
