package bitewish.domain.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

@Service
public class DateServiceImpl implements DateService {
	private final DateTimeFormatter formatter = DateTimeFormatter
			.ofPattern("dd/MM/yyyy");

	private final DateTimeFormatter formatterWithTime = DateTimeFormatter
			.ofPattern("dd/MM/yyyy HH:mm");

	@Override
	public LocalDate getLocalDateFromString(String stringDate) {
		return LocalDate.parse(stringDate, formatter);
	}

	@Override
	public String getStringFromLocalDate(LocalDate localDate) {
		return formatter.format(localDate);
	}

	@Override
	public LocalDate getMondayOfTheWeek(LocalDate anyDate) {
		return getDayOfWeekFromDate(anyDate, 1);
	}

	@Override
	public LocalDate getFridayOfTheWeek(LocalDate anyDate) {
		return getDayOfWeekFromDate(anyDate, 5);
	}

	private LocalDate getDayOfWeekFromDate(LocalDate anyDate, int dayWanted) {
		DayOfWeek weekDayOfDate = anyDate.getDayOfWeek();
		int weekDayValue = weekDayOfDate.getValue();
		if (weekDayValue == dayWanted)
			return anyDate;
		if (weekDayValue > dayWanted)
			return anyDate.minusDays(weekDayValue - dayWanted);
		return anyDate.plusDays(dayWanted - weekDayValue);
	}

	@Override
	public LocalDateTime getLocalDateTimeFromString(String stringLocalDateTime) {
		return LocalDateTime.parse(stringLocalDateTime, formatterWithTime);
	}

	@Override
	public String getStringFromLocalDateTime(LocalDateTime localDateTime) {
		return formatterWithTime.format(localDateTime);
	}
}
