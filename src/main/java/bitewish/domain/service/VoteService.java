package bitewish.domain.service;

import java.util.Optional;

import bitewish.domain.model.BitePoll;
import bitewish.domain.model.Place;
import bitewish.domain.model.User;
import bitewish.domain.model.Vote;

public interface VoteService {
	Vote createVote(BitePoll bitePoll, User votingUser, Place wish);

	Optional<Vote> getUserCurrentVote(User aUserThatVoted,
			BitePoll aPollToVerify);

	Long countVotesFromDate(String date);
}
