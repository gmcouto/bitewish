package bitewish.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bitewish.domain.model.BitePoll;
import bitewish.domain.model.Place;
import bitewish.domain.repository.PlaceRepository;

@Service
public class PlacesServiceImpl implements PlacesService {
	@Autowired
	PlaceRepository placeRepository;

	@Autowired
	DateService dateService;

	@Autowired
	BitePollsService bitePollsService;

	@Override
	public Place createPlace(String name) {
		Place newPlace = new Place(name);
		return placeRepository.save(newPlace);
	}

	@Override
	public Place getOnePlace(Long place_id) {
		return placeRepository.findOne(place_id);
	}

	@Override
	public List<Place> getAllPlacesAvailableForVoting(String date) {
		List<Place> places = placeRepository.findAllByOrderByNameAsc();
		List<BitePoll> pollsFromWeek = bitePollsService.getPollsFromWeek(date);
		pollsFromWeek.forEach(poll -> {
			if (poll.getGrantedWish() != null)
				places.remove(poll.getGrantedWish());
		});
		return places;
	}

}
