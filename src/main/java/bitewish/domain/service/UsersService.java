package bitewish.domain.service;

import bitewish.domain.model.User;

public interface UsersService {
	public User createUser(String name);

	public Long userCount();
}
