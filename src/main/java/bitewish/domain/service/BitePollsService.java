package bitewish.domain.service;

import java.util.List;

import bitewish.domain.model.BitePoll;
import bitewish.domain.model.BitePollResult;

public interface BitePollsService {

	public BitePoll getOneAndCreateIfDoesntExists(String stringDay);

	public BitePoll calculateWinner(String stringDay);

	public List<BitePoll> getPollsFromWeek(String stringDay);

	public List<BitePollResult> getBitePollPartialResult(String stringDay);
}
