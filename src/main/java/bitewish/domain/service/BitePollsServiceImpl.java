package bitewish.domain.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bitewish.domain.model.BitePoll;
import bitewish.domain.model.BitePollResult;
import bitewish.domain.repository.BitePollRepository;

@Service
public class BitePollsServiceImpl implements BitePollsService {
	@Autowired
	BitePollRepository bitePollRepository;

	@Autowired
	DateService dateService;

	@Override
	public BitePoll getOneAndCreateIfDoesntExists(String stringDay) {
		LocalDate day = dateService.getLocalDateFromString(stringDay);
		BitePoll result = bitePollRepository.findFirstByDate(day);
		if (result == null) {
			result = new BitePoll(day);
			result = bitePollRepository.save(result);
		}
		return result;

	}

	@Override
	public BitePoll calculateWinner(String stringDay) {
		LocalDate day = dateService.getLocalDateFromString(stringDay);
		BitePoll result = bitePollRepository.findFirstByDate(day);
		if (result == null)
			return null;
		List<BitePollResult> pollResults = bitePollRepository
				.findBitePollResults(day);
		if (!pollResults.isEmpty())
			result.setGrantedWish(pollResults.get(0).getPlace());
		return bitePollRepository.save(result);
	}

	@Override
	public List<BitePoll> getPollsFromWeek(String stringDay) {
		LocalDate day = dateService.getLocalDateFromString(stringDay);
		LocalDate monday = dateService.getMondayOfTheWeek(day);
		LocalDate friday = dateService.getFridayOfTheWeek(day);
		List<BitePoll> weekPolls = bitePollRepository.findByDateBetween(monday,
				friday);
		return weekPolls;
	}

	@Override
	public List<BitePollResult> getBitePollPartialResult(String stringDay) {
		return bitePollRepository.findBitePollResults(dateService
				.getLocalDateFromString(stringDay));
	}

}
